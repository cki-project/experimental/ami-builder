#!/bin/bash

# Install Ansible.
echo "Installing Ansible with dnf..."
for i in {0..5}; do
  sudo dnf -y -q install ansible && break
done
